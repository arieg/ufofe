#!/bin/sh

die () {
	echo "$@" 1>&2
	exit 1
}
echo "Regenerating configure with autoconf 2.60"
echo autoconf --warnings=all -o ../configure configure.ac || die "autoconf failed"
autoconf --warnings=all -o ../configure configure.ac || die "autoconf failed"
cp ../configure ../configure.bak
cd ..
rm -f autoconf/configure.tmp.ac configure.bak
exit 0
