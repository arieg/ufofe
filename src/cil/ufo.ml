open Cil

(** true if nondet functions should be marked as const/readnone *)
let nondetReadnone = ref false
(** true if functions should be marked with always_inline attribute *)
let alwaysInline = ref false
(** true if should insert special function to mark lines *)
let markLines = ref false

(* ordering of varinfo based on vid. this is used for maps and sets
   over varinfo as key *)
module ViOrder =
struct
  type t = varinfo
  let compare (v1:varinfo) (v2:varinfo) : int =
    if(v1.vid = v2.vid) then 0
    else if (v1.vid < v2.vid) then -1
    else 1
end

(* map with varinfo key *)
module VM = Map.Make(ViOrder)



module type ARGS = sig
  val file : file
end

module Ufo 
  (Args : ARGS)
  =
struct

  class stripInlineAsm = object (self)
    inherit nopCilVisitor as super
    method vinst (instr : instr) =
      begin
	match instr with
	  | Asm (_, _, _, _, _, _) -> 
	    ChangeTo ([])
	  | _ -> SkipChildren
      end
  end

  let stubPrefix = "__UFO__stub__"  
  let file = Args.file

  let addrStubs : varinfo VM.t ref = ref VM.empty

  class addrOfStub = object (self) 
    inherit nopCilVisitor as super
      
    method vexpr (exp : exp) = 
      begin
	match exp with
	  | AddrOf (Var (v), offset) when  VM.mem v !addrStubs ->
	    ChangeTo (AddrOf (Var (VM.find v !addrStubs), offset))
	  | _ -> DoChildren
      end
  end

  (** Returns a varinfo of a function that returns a non-deterministic value 
      of a given type *)
  let nondetFn  (typ:typ) : varinfo =
    let tyname = 
      match typ with
	| TInt (IChar, _) -> "char"
	| TInt (ISChar, _) -> "schar"
	| TInt (IUChar, _) -> "uchar"
	| TInt (IBool, _) -> "_Bool"
	| TInt (IInt, _) -> "int"
	| TInt (IUInt, _) -> "uint"
	| TInt (IShort, _) -> "short"
	| TInt (IUShort, _) -> "ushort"
	| TInt (ILong, _) -> "long"
	| TInt (IULong, _) -> "ulong"
	| TInt (ILongLong, _) -> "long_long"
	| TInt (IULongLong, _) -> "ulong_long"
	| TFloat (FFloat, _) -> "float"
	| TFloat (FDouble, _) -> "double"
	| TFloat (FLongDouble, _) -> "long_double"
	| TNamed (tyinfo, _) -> tyinfo.tname
	| _ -> invalid_arg "Unsupported type" 
	    
    in
    let a = if !nondetReadnone then [Attr ("aconst", [])] else [] in 
    findOrCreateFunc 
      file 
      ("nondet_" ^ tyname) 
      (TFun (typ, Some [], false, a))  
      

  (* with Invalid_argument _ ->  *)
  (*   (\* treat unknown types as integers *\) *)
  (*   findOrCreateFunc  *)
  (* 	file  *)
  (* 	("nondet_int")  *)
  (* 	(TFun (intType, Some [], false, [])) *)

  (** add definition of assert *)
  (*let assertFn = 
    let assertFn' = emptyFunction "assert" in
    let assertFnTy = TFun (Cil.voidType, Some [], false, []) in
    setFunctionTypeMakeFormals assertFn' assertFnTy ;
    let v = makeFormalVar assertFn' "v" (TInt (IBool, [])) in
    let x = ref dummyStmt in
    let errorLoop = mkStmt (Goto (x, builtinLoc)) in x := errorLoop;
    errorLoop.labels <- [Label ("ERROR", builtinLoc, false)] ;
    let body = If (Lval (var v), mkBlock [], mkBlock [errorLoop], builtinLoc) in
    assertFn'.sbody <- mkBlock [mkStmt body] ;
    file.globals <- GFun (assertFn', builtinLoc) :: file.globals ;
    assertFn'.svar *)

	
    
  let exitFnType = TFun (voidType, Some ["a", intType, []], false, [])

  (** exit() function *)
  let exitFn = 
    let fv = findOrCreateFunc file "exit" exitFnType in
    (** cleanup the declaration to make GCC happy *)
    fv.vattr <- [Attr ("noreturn", [])];
    fv.vtype <- exitFnType ; 
    (** ensure exit() is declared up front. Multiple declarations are ok. *)
    file.globals <- GVarDecl (fv, builtinLoc) :: file.globals ;
    fv


  (** function to replace exit() with *)
  let ufoExitStub =
    let fd = Cil.emptyFunction "__UFO_exit_stub" in
    Cil.setFunctionTypeMakeFormals fd exitFnType; 
    let x = ref dummyStmt in
    let loop = mkStmt (Goto (x, builtinLoc)) in x := loop;
    let name = "__UFO_exit_stub_loop" in 
    loop.labels <- [Label (name, builtinLoc, false)];
    fd.sbody.bstmts <- [loop];
    file.globals <- (GFun (fd, Cil.builtinLoc)) :: file.globals ;
    fd.svar

  (** directs goto to ERROR to special exit location *)
  class errorToExit = object (self)
    inherit nopCilVisitor as super
    
    method vstmt (stmt : stmt) = 
      begin
	let eLabel lbl = 
	  match lbl with 
	    | Label (name, _, _) when name = "ERROR" -> true
	    | _ -> false
	in 
	if List.exists eLabel stmt.labels then 
	  begin 
	    let exitCall = Call (None, Lval (var exitFn), 
				 [integer 42], builtinLoc) in
	    
	    stmt.skind <- Instr [exitCall];
	    SkipChildren
	  end 
	else DoChildren
      end

    (** Replace calls to exit() with calls to __UFO_exit_stub() *)
    method vinst (instr : instr) = 
      begin
	match instr with
	  | Call (None, Lval (Var (fv), _), args, loc) when
	      fv.vname = "exit" -> 
	    ChangeTo ([Call (None, Lval (var ufoExitStub), args, loc)])
	  | _ -> SkipChildren
      end
  end

  let labelCount = ref 0
    
  (** removes return statements *)
  class noReturn = object (self)
    inherit nopCilVisitor as super
    
    method vstmt (stmt : stmt) = begin
      match stmt.skind with
	| Return (_, loc) -> 
	  let loop = Goto (ref stmt, loc) in
	  stmt.skind <- loop;
	  if List.length stmt.labels = 0 then
	    begin
	      let name = "__UFO__" ^ (string_of_int !labelCount) in
	      stmt.labels <- [Label (name, builtinLoc, false)];
	      labelCount := !labelCount + 1
	    end ;
	  SkipChildren
	| _ -> DoChildren
    end
  end

  let fdeclTy = TFun (voidType, Some [("a", charPtrType, [])], false, [])
  let fdeclFn = findOrCreateFunc file "__UFO_fdecl" fdeclTy

  let linerTy = 
    let args = [("line", longType, []);
		("file", charPtrType, []);
		("scope", charPtrType, [])] in
    TFun (voidType, Some args, false, [])
  let linerFn = findOrCreateFunc file "__UFO_liner" linerTy 

  let lineNumberCall (loc : location) (scope: string) : stmt =
    let args = 
      [integer loc.line;
       mkString loc.file;
       mkString scope
      ]
    in
    let i = Call (None, Lval (var linerFn), args, builtinLoc) in
    mkStmtOneInstr i

  let fdeclMarkCall (fd : fundec) (loc: location): stmt = 
    lineNumberCall loc ("enter: " ^ fd.svar.vname)
    
  let getStmtLoc (s : stmt) : location = 
    if List.length s.labels > 0 then
      match List.hd s.labels with
	| Label (_, loc, _) -> loc 
	| Case (_, loc) -> loc
	| Default (loc) -> loc
    else
      get_stmtLoc s.skind
	  
  (** inserts marker functions to indicate interesting lines *)
  class lineInserter fd =  let scope = "in: " ^ fd.svar.vname in
  object (self)
    inherit nopCilVisitor as super
    method vstmt (stmt: stmt) = 
      begin
	let getBlockLoc b def_loc = 
	  match b.bstmts with
	    | hd::tl -> getStmtLoc hd
	    | _ -> def_loc 
	in
	(match stmt.skind with
	  | If (_, thenSide, elseSide, loc) -> 
	    thenSide.bstmts <- 
	      (let thenLoc = getBlockLoc thenSide loc in
	       (lineNumberCall  thenLoc (scope ^ " THEN")) :: thenSide.bstmts) ;
	    elseSide.bstmts <- 
	      (let elseLoc = getBlockLoc elseSide loc in
	       (lineNumberCall elseLoc (scope ^ " ELSE")) :: elseSide.bstmts)  
	  | _ -> ()) ;
	
	if List.length stmt.labels > 0 then
	  let replace_with (s) = 
	    let loc = getStmtLoc s in
	    let ln = lineNumberCall loc scope in
	    let skind = s.skind in
	    s.skind <- ln.skind ; ln.skind <- skind ;
	    let newblock = mkBlock [stmt ; ln] in
	    mkStmt (Block (newblock)) 
	  in
	  ChangeDoChildrenPost (stmt, replace_with)
	else if false then
	  match stmt.skind with 
	    | Loop (_, loc, _, _) -> 
	      let replace_with (s) = 
		let ln = lineNumberCall loc scope in
		let newblock = mkBlock [ln ; stmt] in
		mkStmt (Block (newblock)) in
	      ChangeDoChildrenPost (stmt, replace_with)
	    | _ ->  DoChildren
	else
          match stmt.skind with
            | Return (_, loc) ->
              let mark = (lineNumberCall loc ("exit: " ^ fd.svar.vname)) in
              ChangeTo (mkStmt (Block (mkBlock [ mark ; stmt])))
            | _ ->  DoChildren
      end 
  end

  let isPrimitiveTy t =
    match unrollType t with 
      | TInt (_,_)
      | TFloat (_, _)
      | TEnum (_,_)
      | TPtr (_,_) -> true
      | _ -> false 
	
  let isBadTy t = 
    match unrollType t with
      | TBuiltin_va_list (_) -> true
      | TArray (_, _, _) -> true
      | _ -> false
  let havocLocals fd = 
    List.iter 
      (fun local -> 

	 let havoc = 
	   match local.vtype with 
	     | TInt (_, _) -> Some (nondetFn local.vtype)
	     | TFloat (_, _) -> Some (nondetFn local.vtype)
	     | TNamed (t, _) when not (isBadTy local.vtype) -> 
	       Some (nondetFn local.vtype)
	     | TEnum (_, _) -> Some (nondetFn intType)
	     | TPtr (_, _) -> Some (nondetFn longType)
	     | _ -> None
	 in
	   match havoc with 
	     | Some nd ->
		 let havoc = Formatcil.cStmt
		   " loc = nondet (); "
		   (fun n t -> assert false)
		   builtinLoc
		   [
		     ("loc", Fv local);
		     ("nondet", Fv nd)
		   ]
		 in
		   fd.sbody.bstmts <- havoc :: fd.sbody.bstmts
	     | _ -> ()
      )

      fd.slocals

  let run () : unit = 
    (** declare malloc *)
    let mallocTy = 
      TFun (voidPtrType, Some [("sz", ulongType, [])], false, [])     in
    let malloc = findOrCreateFunc file "malloc" mallocTy in
    (** if malloc was declared in other ways, redeclare it properly *)
    malloc.vtype <- mallocTy ;

    (* create dummies for functions whose address is taken *)
    file.globals <- foldGlobals 
      file 
      (fun res g -> 
	match g with
	  | GVarDecl (vi, loc) 
	      when (vi.vaddrof && not (VM.mem vi !addrStubs)) ->
	    (match vi.vtype with 
	      | TFun (_, _, _, _) -> 
		let stub = copyVarinfo vi (stubPrefix ^ vi.vname) in
		stub.vstorage <- Extern ; 
		addrStubs := VM.add vi stub !addrStubs ;
		res @ (GVarDecl (stub, loc) :: [g])
	      | _ -> res @ [g])
	    
	  | GFun (fd, loc) when 
	      (fd.svar.vaddrof && not (VM.mem fd.svar !addrStubs)) ->
	    let stub = copyVarinfo fd.svar (stubPrefix ^ fd.svar.vname) in
	    stub.vstorage <- Extern ; 
	    addrStubs := VM.add fd.svar stub !addrStubs ;
	    res @ (GVarDecl (stub, loc) :: [g])

	  | _ -> res @ [g]
      )
      []
    ;
    let sia = new stripInlineAsm in
    let aos = new addrOfStub in
    let etoe = new errorToExit in
    let noret = new noReturn in
    visitCilFileSameGlobals aos file ;
    visitCilFileSameGlobals sia file ;
    iterGlobals 
      file
      (fun g -> 
	match g with
	  | GFun (fd, loc) when fd.svar.vname = "main" -> 
	      (match fd.svar.vtype  with 
		 | TFun (_, l, b, a) -> fd.svar.vtype <- TFun (intType, l, b, a)
		 | _ -> ());
	    (if !markLines && (List.length fd.sbody.bstmts) > 0 then
		let fdecl = fdeclMarkCall fd loc in 
		fd.sbody.bstmts <- fdecl :: fd.sbody.bstmts ) ;
	    let liner = if !markLines then new lineInserter fd
	      else new nopCilVisitor in
	    let b1 = visitCilBlock liner fd.sbody in
	    let b2 = visitCilBlock etoe b1 in
	    let b3 = visitCilBlock noret b2  in
	    (* fd.sbody <- visitCilBlock aos b3 ;*)
	    fd.sbody <- b3 ; 
	    
	    (* add a loop at the end of main in case it had no return *)
	    let x = ref dummyStmt in 
	    let loop = mkStmt (Goto (x, builtinLoc)) in
	    x := loop ; 
	    let name = "__UFO__" ^ (string_of_int !labelCount) in
	    loop.labels <- [Label (name, builtinLoc, false)];
	    labelCount := !labelCount + 1;
	    fd.sbody.bstmts <- fd.sbody.bstmts @ [loop];

	    havocLocals fd
	    
	  | GFun(fd, loc) -> 
	    (if !markLines && (List.length fd.sbody.bstmts) > 0 then
		let fdecl = fdeclMarkCall fd loc in 
		fd.sbody.bstmts <- fdecl :: fd.sbody.bstmts ) ;
	    let liner = if !markLines then new lineInserter fd
	      else new nopCilVisitor in
	    let b1 = visitCilBlock liner fd.sbody in
	    let b2 = visitCilBlock etoe b1 in
	    (* fd.sbody <- visitCilBlock aos b2 ; *)
	    fd.sbody <- b2 ;
	    havocLocals fd 
	    ;
	    (match fd.svar.vtype with
	      | TFun (rt, args, vararg, attrs) when !alwaysInline ->
		let a = Attr ("always_inline", []) in
		let newAttrs = addAttribute a attrs in
		 fd.svar.vtype <- TFun (rt, args, vararg, newAttrs) 
	      | _ -> ())
	      
	  | GVarDecl (vd, _) 
	      when !nondetReadnone && 
		Util.hasPrefix  "__VERIFIER_nondet" vd.vname ->
	    begin match vd.vtype with
	      | TFun (x, l, b, a) -> 
		let ca = Attr ("aconst", []) in
		vd.vtype <- TFun (x, l, b, addAttribute ca a)
	      | _ -> ()
	    end
	  | GVarDecl (vd, _) when vd.vname = "__CPROVER_assume" ->
	    vd.vname <- "__VERIFIER_assume"
	  | _ -> ()
      ) 
end;;







let feature : featureDescr = 
  { fd_name = "ufo";
    fd_enabled = ref false;
    fd_description = " UFO instrumentations" ;
    fd_extraopt = 
      [
	"--ufo-nondet-readnone", Arg.Set nondetReadnone, 
	" mark nondet() functions as readnone/const";
	"--ufo-always-inline", Arg.Set alwaysInline, 
	" mark functions with always_inline attribute";
	"--ufo-mark-lines", Arg.Set markLines, 
	" mark source lines using a dummy function";
      ];
    fd_doit = (fun f -> 
      let module M = Ufo (struct let file = f end) in
      M.run ()
    );
    fd_post_check = true;
  } 

