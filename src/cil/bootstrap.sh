#!/bin/bash

## configure and build CIL

(cd ../../tools/cil/ && \
    ./configure EXTRASRCDIRS=../../src/cil EXTRAFEATURES="ufo" && \
    make
)