#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Support/Compiler.h"
#include "llvm/PassManager.h"

#include "llvm/Support/IRBuilder.h"

#include "boost/format.hpp"

#include <map>

using namespace llvm;

namespace llvm
{
  class NondetInit : public ModulePass 
  {

  private:
    /** map for nondet functions */
    std::map<const Type*,Constant*> ndfn;
    Module* m;
    
    Constant* getNondetFn (const Type *type)
    {
      Constant* res = ndfn [type];
      if (res == NULL)
	{
	  res = m->getOrInsertFunction 
	    (boost::str 
	     (boost::format ("ufo.nondet.%d") % ndfn.size ()), type, NULL);
	  
	  // -- say that f does not access memory will make llvm
	  // -- assume that all calls to it return the same value
	  // if (Function *f = dyn_cast<Function>(res))
	  //   f->setDoesNotAccessMemory (true);
	  
	  ndfn[type] = res;
	}
      return res;
    }

  public:
    static char ID;
    NondetInit() : ModulePass(&ID), m(NULL) {}    

    
    virtual bool runOnModule(Module &M) 
    {
      
      m = &M;
      bool Changed = false;

      //Iterate over all functions, basic blocks and instructions.
      for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI)
	Changed |= runOnFunction (*FI);
	  
      ndfn.clear ();
      m = NULL;
      
      return Changed;
    }

    bool runOnFunction (Function &F)
    {
      bool Changed = false;
      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b)
	for (BasicBlock::iterator it = b->begin(), ie = b->end(); 
	     it != ie; ++it) 
	  {
	    User* u = &(*it);

	    if (PHINode* phi = dyn_cast<PHINode> (u))
	      {
		for (unsigned i = 0; i < phi->getNumIncomingValues (); i++)
		  {
		    if (UndefValue *uv = 
			dyn_cast<UndefValue> (phi->getIncomingValue (i)))
		      {
			//BasicBlock* inbb = phi->getIncomingBlock (i);
			Constant *ndf = getNondetFn (uv->getType ());
			IRBuilder<> Builder (F.getContext ());
			//Builder.SetInsertPoint (inbb, --inbb->end ());
			Builder.SetInsertPoint (&F.getEntryBlock (), 
						F.getEntryBlock ().begin ());
			phi->setIncomingValue (i, Builder.CreateCall (ndf));
			Changed = true;
		      }
		  }
		
		continue;
	      }

	    // -- the normal case

	    for (unsigned i = 0; i < u->getNumOperands (); i++)
	      {
		if (UndefValue *uv = dyn_cast<UndefValue> (u->getOperand (i)))
		  {
		    Constant *ndf = getNondetFn (uv->getType ());
		    IRBuilder<> Builder (F.getContext ());
		    Builder.SetInsertPoint (&F.getEntryBlock (), 
					    F.getEntryBlock ().begin ());
		    // Builder.SetInsertPoint (b, it);
		    u->setOperand (i, Builder.CreateCall (ndf));
		    Changed = true;
		  }
	      }
	  }
      return Changed;
    }
    

    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesCFG ();
    }
    
  };
  
  char NondetInit::ID = 0;
}



static RegisterPass<NondetInit> X("nondet-init", 
				  "Store nondet() to every alloca address");

