#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Support/Compiler.h"
#include "llvm/PassManager.h"

#include "llvm/Support/IRBuilder.h"

#include "boost/format.hpp"

#include <map>
#include <list>

using namespace llvm;

namespace 
{
  class KillUnusedNondet : public FunctionPass
  {

  public:
    static char ID;
    KillUnusedNondet() : FunctionPass(&ID) {}    


    bool runOnFunction (Function &F)
    {
      std::list<CallInst*> toerase;
      
      bool Changed = false;
      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b)
	for (BasicBlock::iterator it = b->begin(), ie = b->end(); 
	     it != ie; ++it) 
	  {
	    User* u = &(*it);
	    // -- looking for empty users
	    if (!u->use_empty ()) continue;
	    
	    if (CallInst *ci = dyn_cast<CallInst> (u))
	      {
		Function *f = ci->getCalledFunction ();
		if (f == NULL) continue;
		
		if (f->getName ().find ("nondet") != StringRef::npos)
		  toerase.push_back (ci);
	      }
	  }
      
      if (!toerase.empty ()) Changed = true;
      for (std::list<CallInst*>::iterator it = toerase.begin (), 
	     end = toerase.end (); it != end; ++it)
	(*it)->eraseFromParent ();
      
      return Changed;
    }
    

    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesCFG ();
    }
    
  };
  
  char KillUnusedNondet::ID = 0;
}


static RegisterPass<KillUnusedNondet> X("kill-nondet", 
				      "Kill unused nondet calls");

