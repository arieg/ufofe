#!/usr/bin/env python

import sys
import os
import os.path
import atexit
import tempfile
import shutil
import subprocess as sub
import threading
import signal
import ufo as ufo

from ufo import defOptBcName

root = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
verbose = True


def initProfiles():
    profiles = dict()
    base = ['--ufo-increfine=REF5', '--ufo-consrefine=true',
            '--ufo-dvo=true', '--ufo-simplify=false', '--ufo-false-edges=true' ]
    lbase = ['--ufo-widen-step=3'] + base
    profiles ['boxesO0'] = ['-O0', '--ufo-post=BOXES' ] + lbase
    profiles ['boxesO3'] = ['-O3', '--ufo-post=BOXES', '--cpu=20', '--ufo-use-ints'] + lbase
    profiles ['boxO0'] = ['-O0', '--ufo-post=BOX', '--ufo-use-ints'] + lbase
    profiles ['boxO3'] = ['-O3', '--ufo-post=BOX', '--ufo-use-ints'] + lbase
    profiles ['noneO3'] = ['-O3', '--ufo-post=NONE', '--ufo-use-ints'] + base
    profiles ['bpredO3'] = ['-O3', '--ufo-post=BPRED', '--cpu=20', '--ufo-use-ints'] + base
    profiles ['cpredO3'] = ['-O3', '--ufo-post=CPRED', '--ufo-use-ints'] + base
    profiles ['bigwO3'] = ['-O3', '--ufo-post=BOXES', 
                           '--ufo-widen-step=17', '--cpu=10', '--ufo-use-ints'] + base
    profiles ['bigwO0'] = ['-O0', '--ufo-post=BOXES', 
                           '--ufo-widen-step=17', '--ufo-use-ints'] + base

    profiles ['boxbpredO3'] = ['-O3', '--ufo-post=BOXBPRED', '--cpu=60', '--ufo-use-ints'] + lbase
    profiles ['boxbpredO3'].remove ('--ufo-consrefine=true')
    profiles ['boxbpredO3'].append ('--ufo-consrefine=false')

    return profiles

profiles = initProfiles ()

def listProfiles ():
    for (k, v) in profiles.iteritems ():
        print k, ':', ' '.join (v)

def isexec (fpath):
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 

def parseOpt (argv):
    from optparse import OptionParser
    
    parser = OptionParser ()
    parser.add_option ("--save-temps", dest="save_temps",
                       help="Do not delete temporary files",
                       action="store_true",
                       default=False)
    parser.add_option ("--temp-dir", dest="temp_dir",
                       help="Temporary directory",
                       default=None)
    parser.add_option ('--cpu', type='int', dest='cpu',
                       help='CPU time limit (seconds)', default=-1)
    parser.add_option ('--mem', type='int', dest='mem',
                       help='MEM limit (MB)', default=-1)

    parser.add_option ('-m', type=int, dest='arch', default=32,
                       help='Machine architecture 32 or 64')
    parser.add_option ('--profiles', '-p', dest='profiles', 
                       default='bigwO3:cpredO3:boxesO3:boxO3:bpredO3:boxbpredO3:boxesO0',
                       help='Colon separated list of profiles')
    parser.add_option ('--list-profiles', dest='list_profiles',
                       action='store_true', default=False)
    parser.add_option ('--cex', dest='cex', default=None,
                       help='Destination for a counterexample file')

    (options, args) = parser.parse_args (argv)

    if options.list_profiles:
        listProfiles ()
        sys.exit (0)

    if options.arch != 32 and options.arch != 64:
        parser.error ('Unknown architecture {}'.format (opt.arch))
        
    if options.cex != None and os.path.isfile (options.cex):
        os.remove (options.cex)

    return (options, args)

def createWorkDir (dname = None, save = False):    
    if dname == None:
        workdir = tempfile.mkdtemp (prefix='ufopar-')
    else:
        workdir = dname

    if verbose: print "Working directory", workdir

    if not save: atexit.register (shutil.rmtree, path=workdir)
    return workdir

def getUfoPy ():
    ufo = os.path.join (root, "bin/ufo.py")
    if not isexec (ufo):
        raise IOError ("Cannot find ufo")
    return ufo

def cat (in_file, out_file): out_file.write (in_file.read ())

running = list()

def runUfo (args, fname, stdout, stderr):
    O = 3
    if '-O3' in args: O = 3
    elif '-O2' in args: O = 2
    elif '-O1' in args: O = 1
    elif '-O0' in args: O = 0

    in_name = ufo.defOptBcName (fname, optLevel=O)
    if not os.path.isfile (in_name):
        ufo.llvmOpt (fname, in_name, opt_level=O)

    args = args + [in_name]
    if verbose: print ' '.join (args)
    return sub.Popen (args,
                      stdout=open (stdout, 'w'),
                      stderr=open (stderr, 'w'))

    
def runPP (workdir, in_name,  arch=32):
    cpp_out = ufo.defCppName (in_name, workdir)
    ufo.cpp (in_name, cpp_out, arch=arch)
    in_name = cpp_out
    
    ufo_out = ufo.defUfoName (in_name)
    ufo.cilly (in_name, ufo_out, arch=arch, mark_lines=False)
    in_name = ufo_out

    bc = ufo.defBcName (in_name)
    ufo.llvmGcc (in_name, bc, arch=arch)
    in_name = bc

    obc = ufo.defOBcName (in_name, workdir)
    ufo.llvmOptBase (in_name, obc)

    return obc

    
def run (workdir, fname, ufo_args = [], profs = [], 
         cex = None, arch=32, cpu=-1, mem=-1):

    print "BRUNCH_STAT Result UNKNOWN"
    sys.stdout.flush ()

    obc_name = runPP (workdir, fname, arch=arch)

    ### remove all optimized versions if they exist
    # for i in range (0,4):
    n = defOptBcName (obc_name, optLevel=3)
    if os.path.isfile (n): os.remove (n)

    base_args = [getUfoPy (), '--mem={}'.format(mem), 
                 '-m{}'.format (arch)]
    base_args.extend (ufo_args)

    if cex != None:
        cex_base = os.path.basename (fname)
        cex_base = os.path.splitext (cex_base)[0]
        cex_base = os.path.join (workdir, cex_base)


    conf_name = list ()
    ufo = list ()
    for prof in profs:
        conf_name.append (prof)
        p_args = base_args + profiles [prof]
        if cex != None:
            cex_name = '{}.{}.trace'.format (cex_base, prof)
            p_args.append ('--cex={}'.format (cex_name))
        ufo.append (p_args)

    name = os.path.splitext (os.path.basename (fname))[0]
    stdout = [os.path.join (workdir, name + '_ufo{}.stdout'.format (i)) 
              for i in range(len (ufo))]
    stderr = [os.path.join (workdir, name + '_ufo{}.stderr'.format (i))
              for i in range (len (ufo))]
    
    global running
    running.extend ([runUfo (ufo [i], obc_name, stdout[i], stderr [i])
                     for i in range (len (ufo))])


    orig_pids = [p.pid for p in running]
    pids = [p.pid for p in running ]
    pid = -1
    returnvalue = -1
    while len (pids) != 0:
        print 'Running: ', pids
    
        (pid, returnvalue, ru_child) = os.wait4 (-1, 0)

        print 'Finished pid {0} with'.format (pid),
        print ' code {0} and signal {1}'.format((returnvalue // 256), 
                                                (returnvalue % 256))
        pids.remove (pid)
        
        if returnvalue == 0:
            for p in pids:
                try:
                    os.kill (p, signal.SIGTERM)
                except OSError: pass
                finally:
                    try:
                        os.waitpid (p, 0)
                    except OSError: pass
            break
    
    if returnvalue == 0:
        idx = orig_pids.index (pid)
        cat (open (stdout [idx]), sys.stdout)
        cat (open (stderr [idx]), sys.stderr)
        if cex != None:
            cex_name = '{}.{}.trace'.format (cex_base, conf_name [idx])
            print 'Copying {} to {}'.format (cex_name, cex)
	    if os.path.isfile (cex_name):
                shutil.copy2 (cex_name, cex)
                print 'Counterexample trace is in {}'.format (cex)

    
        print 'WINNER: ', ' '.join (ufo [idx])
        print 'BRUNCH_STAT config {0}'.format (idx)
        print 'BRUNCH_STAT config_name {0}'.format (conf_name [idx])
    
    else:  
        print "ALL INSTANCES FAILED"
        print 'Calling sys.exit with {}'.format (returnvalue // 256)
        sys.exit (returnvalue // 256)

    running[:] = []
    return returnvalue

def ufo_opt (x): 
    if x.startswith ('-'):
        y = x.strip ('-')
        return y.startswith ('ufo') or y == 'lawi'
    return False
        
def non_ufo_opt (x): return not ufo_opt (x)


def main (argv):
    os.setpgrp ()
    ufo.loadEnv (os.path.join (ufo.root, "env.common"))

    ufo_args = filter (ufo_opt, argv [1:])
    argv = filter (non_ufo_opt, argv [1:])

    (opt, args) = parseOpt (argv)
    
    workdir = createWorkDir (opt.temp_dir, opt.save_temps)
    returnvalue = 0
    for fname in args:
        returnvalue = run (workdir, fname, ufo_args, opt.profiles.split (':'), 
                           opt.cex, opt.arch, opt.cpu, opt.mem)
    return returnvalue

def killall ():
    global running
    for p in running:
        try:
            if p.poll () == None:
                p.terminate ()
                p.kill ()
                p.wait ()
                # no need to kill pg since it kills its children
        except OSError:   pass
    running[:] = []

if __name__ == '__main__':
    # unbuffered output
    sys.stdout = os.fdopen (sys.stdout.fileno (), 'w', 0)
    try:
        signal.signal (signal.SIGTERM, lambda x, y: killall())
        sys.exit (main (sys.argv))
    except KeyboardInterrupt: pass
    finally: killall ()
            
