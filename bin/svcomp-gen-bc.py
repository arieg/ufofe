#!/usr/bin/env python

import sys
import os
import os.path
import atexit
import tempfile
import shutil
import subprocess as sub
import ufo

root = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
verbose = True

def parseOpt (argv):
    from optparse import OptionParser
    
    parser = OptionParser ()
    parser.add_option ("--save-temps", dest="save_temps",
                       help="Do not delete temporary files",
                       action="store_true",
                       default=False)
    parser.add_option ("--temp-dir", dest="temp_dir",
                       help="Temporary directory",
                       default=None)

    parser.add_option ('-m', type=int, dest='arch', default=32,
                       help='Machine architecture 32 or 64')
    parser.add_option ("--svcomp", dest="svcomp", 
                       help="Location of sv-comp", default=None)
    parser.add_option ('-d', "--dst", dest="dst", 
                       help='Destination directory', default=None)

    (options, args) = parser.parse_args (argv)

    if options.svcomp != None:
        if os.path.isdir (options.svcomp):
            os.environ ['SVCOMP'] = options.svcomp

    if options.arch != 32 and options.arch != 64:
        parser.error ('Unknown architecture {}'.format (opt.arch))

    if options.dst == None:
        parser.error ("Need destination directory")
        
    return (options, args)
   
def loadBench (bench):
    import glob

    files = []
    for line in open (bench):
        x = line.strip ()
        if len (x) == 0: continue
        if not x.startswith ('/'):
            x = os.path.join ("$SVCOMP", x)
        x = os.path.expandvars (x)
        files.extend (glob.glob (x))
    return files

def createWorkDir (dname = None, save = False):    
    if dname == None:
        workdir = tempfile.mkdtemp (prefix='ufopar-')
    else:
        workdir = dname
        
    if verbose: print "Working directory", workdir

    if not save: atexit.register (shutil.rmtree, path=workdir)
    return workdir

def genBc (fname, arch, workdir, dst):
    (dirname, name) = os.path.split (fname)
    dirname = os.path.basename (dirname)

    #out = os.path.join (dst, dirname, name)

    out_dir = os.path.join (dst, dirname)
    if not os.path.exists (out_dir): os.mkdir (out_dir)
    
    in_name = fname

    cpp_out = ufo.defCppName (in_name, workdir)
    ufo.cpp (in_name, cpp_out, arch=arch)
    in_name = cpp_out
    
    ufo_out = ufo.defUfoName (in_name)
    ufo.cilly (in_name, ufo_out, arch=arch, mark_lines=False)
    in_name = ufo_out

    bc = ufo.defBcName (in_name)
    ufo.llvmGcc (in_name, bc, arch=arch)
    in_name = bc

    obc = ufo.defOBcName (in_name, workdir)
    ufo.llvmOptBase (in_name, obc)

    out0 = ufo.defOptBcName (in_name, optLevel=0, wd=out_dir)
    ufo.llvmOpt (obc, out0, opt_level=0)

    out3 = ufo.defOptBcName (in_name, optLevel=3, wd=out_dir)
    ufo.llvmOpt (obc, out3, opt_level=3)    
 
def main (argv):
    ufo.loadEnv (os.path.join (ufo.root, "env.common"))

    (opt, args) = parseOpt (sys.argv[1:])
    workdir = createWorkDir (opt.temp_dir, opt.save_temps)

    for b in args:
        for f in loadBench (b):
            genBc (f, opt.arch, workdir, opt.dst)

    return 0

if __name__ == '__main__':        
    sys.exit (main (sys.argv))
