#!/usr/bin/env python

import sys
import os
import os.path
import subprocess as sub

root = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
verbose = True

def isexec (fpath):
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 

def parseOpt (argv):
    def guessBenchmark (name):
        bname = name
        if os.path.isfile (bname): return bname
        bname += ".bench"
        if os.path.isfile (bname): return bname
        return None

    from optparse import OptionParser
    
    parser = OptionParser ()
    parser.add_option ('--cpu', type='int', dest='cpu',
                       help='CPU time limit (seconds)', default=60)
    parser.add_option ('--mem', type='int', dest='mem',
                       help='MEM limit (MB)', default=4000)
    parser.add_option ('--config', dest='config', 
                       help='Optional configuration file for ufo',
                       default=None)
    parser.add_option ("--svcomp", dest="svcomp", 
                       help="Location of sv-comp", default=None)
    parser.add_option ("--ufo", dest='ufopy', 
                       help='Location of ufo.py executable', 
                       default = None)

    (opt, args) = parser.parse_args (argv)

    if len (args) != 1:
        parser.error ("Must provide a single benchmark")

    bench_name = guessBenchmark (args [0])
    
    if bench_name == None:
        parser.error ("Cannot find benchmark {}".format (args [0]))

    if opt.config == None:
        opt.config = os.path.splitext (bench_name)[0] + ".config"
    
    if not os.path.isfile (opt.config):
        parser.error ("Cannot find configuration: {}".format (opt.config))

    if opt.svcomp != None:
        if os.path.isdir (opt.svcomp):
            os.environ ['SVCOMP'] = opt.svcomp
        else:
            raise IOError ("Cannot find sv-comp at '{}'".format(opt.svcomp))

    if opt.ufopy == None: opt.ufopy = getUfoPy ()
    return (opt, bench_name)

def loadEnv (filename):
    if not os.path.isfile (filename): return

    f = open (filename)
    for line in f:
        sl = line.split('=', 1)
        # skip lines without equality
        if len(sl) != 2: 
            continue
        (key, val) = sl

        os.environ [key] = os.path.expandvars (val.strip ())

def getBrunch ():
    ### XXX can call brunch directly, instead of through the shell
    brunch = os.path.join (root, "bin/brunch.py")
    if not isexec (brunch):
        raise IOError ("Cannot find brunch")
    return brunch

def getUfoPy ():
    ufo = os.path.join (root, "bin/ufo.py")
    if not isexec (ufo):
        raise IOError ("Cannot find ufo")
    return ufo

def loadBench (bench):
    import glob

    files = []
    for line in open (bench):
        x = line.strip ()
        if len (x) == 0: continue
        if not x.startswith ('/'):
            x = os.path.join ("$SVCOMP", x)
        x = os.path.expandvars (x)
        files.extend (glob.glob (x))
    return files
def loadConfig (config):
    opt = []
    for line in open (config):
        opt.extend (line.rstrip ().split (' '))
    return opt

def brunch (ufopy, config, bench, cpu, mem, out_loc = '.'):
    # load bench file and resolve all lines in there
    # create brunch command line
    # call checked
    import datetime as dt
    
    bench_name = os.path.splitext (os.path.basename (bench))[0]

    dt = dt.datetime.now ().strftime ('%d_%m_%Y-t%H-%M-%S')
    out = os.path.join (out_loc, 
                        "out.{b}.date-{dt}".format(b=bench_name,dt=dt))
    os.mkdir (out)
    import shutil
    shutil.copy2 (config, os.path.join (out, os.path.basename (config)))
    shutil.copy2 (bench, os.path.join (out, os.path.basename (bench)))

    fmt="base:Status:Result:Cpu:ufo.total"
    fmt+=":aArg.size:ufo.main_loop:Invariant.add"
    fmt+=":asm_refiner.simplify.assumptions:msat.from_expr:msat.to_expr"
    fmt+=":refiner.assumptions:refiner.assumptions.total"
    fmt+=":refiner.interps:refiner.mkInScope"
    fmt+=":refiner.mkInScope.elim_and_simplify"
    fmt+=":refiner.mkInScope.elim_node:refiner.mkInScope.elim_other"
    fmt+=":refiner.mkInScope.out_of_scope:refiner.mkInScope.propSimp"
    fmt+=":refiner.mkInScope.variants:refiner.mkInScope.z3.forall"
    fmt+=":refiner.nnf:refiner.pre:refiner.total:ufo.expand"
    fmt+=":ufo.expand.is_covered:ufo.post:z3.from_expr:z3.to_expr"
    fmt+=":File:config_name"

    brunch_args = [getBrunch (),
                   '--cpu', str(cpu),
                   '--mem', str(mem),
                   '--out', out,
                   '--format', fmt]
    brunch_args.extend (loadBench (bench))
    brunch_args.extend (['--', ufopy])
    brunch_args.extend (loadConfig (config))
    brunch_args.append ('{f}')

    if verbose:
        print ' '.join (brunch_args)

    sub.check_call (brunch_args)


def main (argv):
    loadEnv (os.path.join (root, "env.common"))
    (opt, bname) = parseOpt (argv [1:])
    
    brunch (opt.ufopy, opt.config, bname, opt.cpu, opt.mem)

    
if __name__ == '__main__':
    # unbuffered output
    sys.stdout = os.fdopen (sys.stdout.fileno (), 'w', 0)
    sys.exit (main (sys.argv))
    
