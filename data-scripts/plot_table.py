#!/usr/bin/python

import sys
import sqlite3 as sql
import numpy as np
import pylab as plt

from optparse import OptionParser


def load_data (db, table, rows="*", order=None):
    c = db.cursor ()
    q = "select {rows} from {name}".format (name=table, rows=rows)
    if not order == None:
        q = q + " order by {order}".format (order=order)
    s = c.execute (q)

    h = [("f{name}".format (name=i), "object") 
         for i in range (len (s.description))]

    data = np.asarray (list (s), dtype=h)
    return data

def plot (data):
    fig = plt.figure ()
    ax = fig.add_subplot (111)
    
    dt = data.dtype
    if len (dt)  == 1:
        ax.plot (data [dt.names [0]])
    else:
        ax.plot (data[dt.names [0]], data[dt.names [1]])
    plt.show ()

def parseOpt ():
    parser = OptionParser ()
    parser.add_option ("--db", dest="db", 
                       help = "Database to use",
                       default = "results.db")
    parser.add_option ("-t", "--table", dest="table",
                       help="Table name", metavar="TABLE")
    parser.add_option ("--rows", dest="rows", help="rows to plot", 
                       default="*")
    parser.add_option ("--order", dest="order", help="order by clause",
                       default=None)

    (options, args) = parser.parse_args ()
    if not len (args) == 0:
        parser.error ("Unknown arguments")
    if options.table == None:
        parser.error ("No table name given")

    return options

def main ():
    opt = parseOpt ()
    db = sql.connect (opt.db)
    
    data = load_data (db, opt.table, opt.rows, opt.order)
    plot (data)
    db.close ()

if __name__ == "__main__":
    sys.exit (main ())
