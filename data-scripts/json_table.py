#!/usr/bin/env python

import sys
import sqlite3 as sql
from optparse import OptionParser

def parseOpt ():
    parser = OptionParser ()
    parser.add_option ("--db", dest="db", 
                       help="Database to use",
                       default="results.db")
    parser.add_option ("-t", "--table", dest="table",
                       help="Table name", metavar="TABLE", 
                       default='t')
    
    (opt, args) = parser.parse_args ()
    if not len (args) == 0: parser.error ("Unknown arguments")
    return opt

def main (argv):
    opt = parseOpt ()
    db = sql.connect (opt.db)

    c = db.cursor ()
    q = "select file, cpu, total, res, config_name from {table} order by file"
    q = q.format(table=opt.table)
    s = c.execute (q)
    
    import json
    # data = dict ()
    # data ['aaData'] = list (s)
    print 'var aDataSet = ', json.dumps (list (s), indent=2), ';'
    
    return 0

if __name__ == '__main__':
    sys.exit (main (sys.argv))
