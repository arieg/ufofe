UFO Front-end
=============

This is the front-end for [UFO][ufo-repo]. It is a collection of CIL and LLVM
passes and Python scripts. 
[ufo-repo]: http://bitbucket.org/arieg/ufo

Directory Structure
===================

  * `bin` contains the main scripts to run ufo
     * `ufo.py` runs UFO on a C file
     * `ufo-svcomp-par.py` runs several UFO instances in parallel
  * `tools` contains third-party tools used
     * `llvm` a custom version of LLVM
     * `cil` CIL
  * `src` contains custom LLVM and CIL passes
     * `cil` the CIL pass
     * `llvm/ufofe` LLVM passes

Compilation
===========

  1. Configure CIL and passes by following `src/cil/README`
  2. Configure LLVM and passes by following `src/llvm/ufofe/README`
  3. Configure top-level with 

        ./configure --with-llvmbin=$LLVM_ROOT --with-llvmgccbin=$LLVM_GCC --with-ufo=$UFO --with-bench=$SVBENCH
   where 

       *  `$LLVM_ROOT` location of LLVM root compiled in step 2, 
       *  `$LLVM_GCC` location of `llvm-gcc`,
       *  `$UFO` location of ufo BINARY
       *  `$SVBENCH` location of SVCOMP benchamrks (can be any directory)

